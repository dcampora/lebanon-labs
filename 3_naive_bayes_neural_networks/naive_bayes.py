import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import time
import csv
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import GaussianNB, BernoulliNB, MultinomialNB

# Start random with a seed, for reproducible results
import random
random.seed(0)

In this lab we will work with the Titanic dataset.
The data of all passengers 
Let's load it and see how it looks like:

Here is a description of every entry in the table:

    Survived - Whether the passenger survived or not
    Pclass - Class of travel
    Age - Age of the passenger
    SibSp - Number of siblings / spouse aboard
    Parch - Number of parents / children aboard
    Fare - Price of the ticket
    Sex - Sex of the passenger
    Embarked - The port in which a passenger has embarked.
               C: Cherbourg, S: Southampton, Q: Queenstown

original_dataset = pd.read_csv("titanic/train.csv")
print(original_dataset)

Before we can work with it, notice how there are various `NaN` (not a number)
values. These would prevent us from doing data analysis, so we will consider
a clean dataset.

Some values, like the Cabin, are unfortunately for the most part NaN.
Therefore, we will keep it out of our training / test sets.

Finally, we will convert some of the features to numerical categories.
Particularly, the feature "Sex" can be `{male, female}` -> `{0, 1}`, and 
"Embarked" can be `{"S", "C", "Q"}` -> `{0, 1, 2}`

# Convert categorical variable to numeric
original_dataset["Sex_categorical"] = \
  np.where(original_dataset["Sex"]=="male", 0, 1)

original_dataset["Embarked_categorical"] = \
  np.where(original_dataset["Embarked"]=="S", 0,
    np.where(original_dataset["Embarked"]=="C", 1,
      np.where(original_dataset["Embarked"]=="Q", 2, 3)
    )
  )

# Cleaning original_dataset of NaN
original_dataset = original_dataset[[
  "Survived",
  "Pclass",
  "Age",
  "SibSp",
  "Parch",
  "Fare",
  "Sex_categorical",
  "Embarked_categorical"
]].dropna(axis=0, how='any')

# Split dataset in training and test datasets
training_dataset, test_dataset = train_test_split(original_dataset,
                                                  test_size=0.2,
                                                  random_state=random.randint(1, 10000))

print(training_dataset)

We are now ready to apply a supervised training classifier.
As a first approximation, we could assume the "Fare" plays
a role in determining whether a passenger would survive.

We can assume the distribution of the data follows a normal distribution.
Let's instantiate a Naive Bayes classifier with a Gaussian distribution function:

gnb = GaussianNB()
used_features =["Fare"]

# Train classifier
gnb.fit(
  training_dataset[used_features].values,
  training_dataset["Survived"]
)
y_pred = gnb.predict(test_dataset[used_features])

# Print results
print("Number of mislabeled points out of a total {} points: {}, test accuracy: {:05.2f}%"
      .format(
          test_dataset.shape[0],
          (test_dataset["Survived"] != y_pred).sum(),
          100*(1-(test_dataset["Survived"] != y_pred).sum() / test_dataset.shape[0])
))

# Unfortunately, our classifier is only able to obtain an accuracy of 65.73%.
# Let's understand what is going on. 

mean_fare_survived = np.mean(test_dataset[test_dataset["Survived"]==1]["Fare"])
std_fare_survived = np.std(test_dataset[test_dataset["Survived"]==1]["Fare"])
mean_fare_not_survived = np.mean(test_dataset[test_dataset["Survived"]==0]["Fare"])
std_fare_not_survived = np.std(test_dataset[test_dataset["Survived"]==0]["Fare"])

print("mean_fare_survived = {:03.2f}".format(mean_fare_survived))
print("std_fare_survived = {:03.2f}".format(std_fare_survived))
print("mean_fare_not_survived = {:03.2f}".format(mean_fare_not_survived))
print("std_fare_not_survived = {:03.2f}".format(std_fare_not_survived))

# TODO: Print the graph with the distributions and the normal distributions on top

# Use other variables in the dataset to improve the accuracy of the classifier.



# from sklearn import datasets
# iris = datasets.load_iris()
# print(iris)

# gnb = GaussianNB()
# y_pred = gnb.fit(iris.data, iris.target).predict(iris.data)
# print("Number of mislabeled points out of a total %d points : %d"
#       % (iris.data.shape[0],(iris.target != y_pred).sum()))


# mean_survival = np.mean(X_train["Survived"])
# mean_not_survival = 1 - mean_survival

# print("Survival prob = {:03.2f}%, Not survival prob = {:03.2f}%".format(100*mean_survival,100*mean_not_survival))

# from sklearn.naive_bayes import GaussianNB
# gnb = GaussianNB()
# used_features =["Fare"]
# y_pred = gnb.fit(X_train[used_features].values, X_train["Survived"]).predict(X_test[used_features])
# print("Number of mislabeled points out of a total {} points : {}, performance {:05.2f}%"
#       .format(
#           X_test.shape[0],
#           (X_test["Survived"] != y_pred).sum(),
#           100*(1-(X_test["Survived"] != y_pred).sum()/X_test.shape[0])
# ))
# print("Std Fare not_survived {:05.2f}".format(np.sqrt(gnb.sigma_)[0][0]))
# print("Std Fare survived: {:05.2f}".format(np.sqrt(gnb.sigma_)[1][0]))
# print("Mean Fare not_survived {:05.2f}".format(gnb.theta_[0][0]))
# print("Mean Fare survived: {:05.2f}".format(gnb.theta_[1][0]))
