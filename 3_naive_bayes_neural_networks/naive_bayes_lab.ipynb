{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Naive Bayes lab\n",
    "\n",
    "In this lab we will explore how the Naive Bayes algorithm works with a real life example."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import matplotlib.patches as mpatches\n",
    "import time\n",
    "import csv\n",
    "from sklearn.model_selection import train_test_split\n",
    "from sklearn.naive_bayes import GaussianNB, BernoulliNB, MultinomialNB\n",
    "import math\n",
    "\n",
    "# Start random with a seed, for reproducible results\n",
    "import random\n",
    "random.seed(0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will employ the Titanic dataset. It includes data for all passengers,\n",
    "including whether they survived the accident or not.\n",
    "\n",
    "Here is a description of every entry in the table:\n",
    "\n",
    "    Survived - Whether the passenger survived or not\n",
    "    Pclass - Class of travel\n",
    "    Age - Age of the passenger\n",
    "    SibSp - Number of siblings / spouse aboard\n",
    "    Parch - Number of parents / children aboard\n",
    "    Fare - Price of the ticket\n",
    "    Sex - Sex of the passenger\n",
    "    Embarked - The port in which a passenger has embarked.\n",
    "               C: Cherbourg, S: Southampton, Q: Queenstown\n",
    "\n",
    "Let's load the dataset and check by ourselves:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "original_dataset = pd.read_csv(\"titanic/train.csv\")\n",
    "print(original_dataset)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Before we can work with it, notice how there are various `NaN` (not a number)\n",
    "values. These would prevent us from doing data analysis, so we will consider\n",
    "a clean dataset.\n",
    "\n",
    "Some values, like the Cabin, are unfortunately for the most part `NaN`.\n",
    "Therefore, we will keep it out of our training / test sets.\n",
    "\n",
    "Finally, we will convert some of the features to numerical categories.\n",
    "Particularly, the feature \"Sex\" can be `{male, female}` -> `{0, 1}`, and \n",
    "\"Embarked\" can be `{\"S\", \"C\", \"Q\"}` -> `{0, 1, 2}`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Convert categorical variable to numeric\n",
    "original_dataset[\"Sex_categorical\"] = \\\n",
    "  np.where(original_dataset[\"Sex\"]==\"male\", 0, 1)\n",
    "\n",
    "original_dataset[\"Embarked_categorical\"] = \\\n",
    "  np.where(original_dataset[\"Embarked\"]==\"S\", 0,\n",
    "    np.where(original_dataset[\"Embarked\"]==\"C\", 1,\n",
    "      np.where(original_dataset[\"Embarked\"]==\"Q\", 2, 3)\n",
    "    )\n",
    "  )\n",
    "\n",
    "# Cleaning original_dataset of NaN\n",
    "original_dataset = original_dataset[[\n",
    "  \"Survived\",\n",
    "  \"Pclass\",\n",
    "  \"Age\",\n",
    "  \"SibSp\",\n",
    "  \"Parch\",\n",
    "  \"Fare\",\n",
    "  \"Sex_categorical\",\n",
    "  \"Embarked_categorical\"\n",
    "]].dropna(axis=0, how='any')\n",
    "\n",
    "# Split dataset in training and test datasets\n",
    "training_dataset, test_dataset = train_test_split(original_dataset,\n",
    "                                                  test_size=0.2,\n",
    "                                                  random_state=random.randint(1, 10000))\n",
    "\n",
    "print(training_dataset)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We are now ready to apply a supervised training classifier.\n",
    "As a first approximation, we could assume the \"Fare\" plays\n",
    "a role in determining whether a passenger would survive.\n",
    "\n",
    "We can assume the distribution of the data follows a normal distribution.\n",
    "Let's instantiate a Naive Bayes classifier with a Gaussian distribution function, and predict using only the \"Fare\" attribute:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gnb = GaussianNB()\n",
    "used_features =[\"Fare\"]\n",
    "\n",
    "# Train classifier\n",
    "gnb.fit(\n",
    "  training_dataset[used_features].values,\n",
    "  training_dataset[\"Survived\"]\n",
    ")\n",
    "y_pred = gnb.predict(test_dataset[used_features])\n",
    "\n",
    "# Print results\n",
    "print(\"Mislabeled {} out of a total of {} points, test accuracy: {:05.2f}%\"\n",
    "      .format(\n",
    "          (test_dataset[\"Survived\"] != y_pred).sum(),\n",
    "          test_dataset.shape[0],\n",
    "          100*(1-(test_dataset[\"Survived\"] != y_pred).sum() / test_dataset.shape[0])\n",
    "))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Unfortunately, our classifier is only able to obtain an accuracy of 65.73%, which is rather poor.\n",
    "\n",
    "Before moving forward, let's understand what is going on. We are currently using a Naive Bayes algorithm with a Gaussian distribution classifier. Such a distribution follows the formula:\n",
    "\n",
    "![title](resources/gaussian_distribution_formula.png)\n",
    "\n",
    "where $\\mu$ and $\\sigma^2$ are the mean and the standard deviation of the dataset respectively. We can find out what are these values for our training dataset for those passengers who survived and didn't survived:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "mean_fare_survived = np.mean(training_dataset[training_dataset[\"Survived\"]==1][\"Fare\"])\n",
    "std_fare_survived = np.std(training_dataset[training_dataset[\"Survived\"]==1][\"Fare\"])\n",
    "mean_fare_not_survived = np.mean(training_dataset[training_dataset[\"Survived\"]==0][\"Fare\"])\n",
    "std_fare_not_survived = np.std(training_dataset[training_dataset[\"Survived\"]==0][\"Fare\"])\n",
    "\n",
    "print(\"mean fare survived = {:03.2f}\".format(mean_fare_survived))\n",
    "print(\"std fare survived = {:03.2f}\".format(std_fare_survived))\n",
    "print(\"mean fare not survived = {:03.2f}\".format(mean_fare_not_survived))\n",
    "print(\"std fare not survived = {:03.2f}\".format(std_fare_not_survived))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can now plot the data in a histogram, to get a grasp of how the data looks:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Helper function to avoid using scipy\n",
    "def normpdf(x, mu, sigma):\n",
    "    u = (x-mu)/abs(sigma)\n",
    "    y = (1/(math.sqrt(2*math.pi)*abs(sigma)))*math.exp(-u*u/2)\n",
    "    return y\n",
    "\n",
    "plt.figure(figsize=(12, 9))\n",
    "\n",
    "# histogram of the survived data\n",
    "n, bins, patches = plt.hist(\n",
    "  training_dataset[training_dataset[\"Survived\"]==1][\"Fare\"],\n",
    "  100,\n",
    "  density=\"density\",\n",
    "  facecolor='blue',\n",
    "  label='survived',\n",
    "  alpha=0.75)\n",
    "\n",
    "# histogram of not survived data\n",
    "n, bins, patches = plt.hist(\n",
    "  training_dataset[training_dataset[\"Survived\"]==0][\"Fare\"],\n",
    "  100,\n",
    "  density=\"density\",\n",
    "  facecolor='orange',\n",
    "  label='not survived',\n",
    "  alpha=0.75)\n",
    "\n",
    "# Some settings for the figure\n",
    "plt.legend(prop={'size': 14})\n",
    "\n",
    "plt.xlabel('Fare ($)')\n",
    "plt.title('Fares of all passengers')\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If we fit Gaussian functions over the `survived` and `not survived` fare subsets, we obtain the following:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Helper function to avoid using scipy\n",
    "def normpdf(x, mu, sigma):\n",
    "    u = (x-mu)/abs(sigma)\n",
    "    y = (1/(math.sqrt(2*math.pi)*abs(sigma)))*math.exp(-u*u/2)\n",
    "    return y\n",
    "\n",
    "plt.figure(figsize=(12, 9))\n",
    "\n",
    "# histogram of the survived data\n",
    "n, bins, patches = plt.hist(\n",
    "  training_dataset[training_dataset[\"Survived\"]==1][\"Fare\"],\n",
    "  100,\n",
    "  density=\"density\",\n",
    "  facecolor='blue',\n",
    "  label='survived',\n",
    "  alpha=0.75)\n",
    "\n",
    "# Gaussian of survived data\n",
    "y = [normpdf(x, mean_fare_survived, std_fare_survived) for x in bins]\n",
    "plt.plot(bins, y, 'b--', linewidth=2, label=\"Gaussian fit for survived\")\n",
    "\n",
    "# histogram of not survived data\n",
    "n, bins, patches = plt.hist(\n",
    "  training_dataset[training_dataset[\"Survived\"]==0][\"Fare\"],\n",
    "  100,\n",
    "  density=\"density\",\n",
    "  facecolor='orange',\n",
    "  label='not survived',\n",
    "  alpha=0.75)\n",
    "\n",
    "# Gaussian of not survived data\n",
    "y = [normpdf(x, mean_fare_not_survived, std_fare_not_survived) for x in bins]\n",
    "plt.plot(bins, y, color=\"red\", linestyle=\"--\", linewidth=2, label=\"Gaussian fit for not survived\")\n",
    "\n",
    "# intersection of two Gaussian curves\n",
    "y1 = [normpdf(x, mean_fare_survived, std_fare_survived) for x in range(500)]\n",
    "y2 = [normpdf(x, mean_fare_not_survived, std_fare_not_survived) for x in range(500)]\n",
    "x_intersection = np.argwhere(np.diff(np.sign(np.array(y1) - np.array(y2)))).flatten()\n",
    "\n",
    "plt.axvline(\n",
    "    x=x_intersection,\n",
    "    label='line at x = {}'.format(x_intersection[0]),\n",
    "    color=\"black\",\n",
    "    linestyle='--')\n",
    "\n",
    "# Some settings for the figure\n",
    "plt.legend(prop={'size': 14})\n",
    "\n",
    "plt.xlabel('Fare ($)')\n",
    "plt.title('Fares of all passengers')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note how the Gaussian functions cross each other at `x = 61`.\n",
    "\n",
    "As we have seen in theory, what makes Naive Bayes _naive_ is that it assumes that all features are independent between themselves:\n",
    "\n",
    "![title](resources/naive_bayes.png)\n",
    "\n",
    "Therefore, if `classifier(Fare) ≥ 61` then `P(Fare | Survival = 1) ≥ P(Fare | Survival = 0)` (the line where the Gaussians cross) and we classify this person as Survival. Else we classify them as Not Survival.\n",
    "\n",
    "It so happens that 65.73% of the __test dataset__ follows this rule, but unfortunately there are many exceptions to the rule!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise 1\n",
    "\n",
    "Use the `Age` feature of the passenger to try and classify whether they survived.\n",
    "\n",
    "* Does it do better or worse than the `Fare`?\n",
    "* Plot the Gaussians and observe the distributions. Does it nicely fit the data?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise 2\n",
    "\n",
    "Use all features to classify the dataset. What accuracy does it obtain on the test dataset?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
