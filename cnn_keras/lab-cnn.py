#!/usr/bin/python3

import numpy as np
import pickle
import matplotlib.pyplot as plt

# CIFAR-10 types
(AIRPLANE, AUTOMOBILE, BIRD, CAT, DEER, DOG, FROG, HORSE, SHIP, TRUCK) = (0, 1, 2, 3, 4, 5, 6, 7, 8, 9)

# Load first data batch
f = open('../cifar-10-batches-py/data_batch_1', 'rb')
data_batch_dict = pickle.load(f, encoding='latin1')
f.close()
x = data_batch_dict['data']
y = data_batch_dict['labels']
x = x.reshape(10000, 3, 32, 32).transpose(0,2,3,1).astype('float')
y = np.array(y)

# Visualize CIFAR 10
# print("Some random pictures")
# fig, axes1 = plt.subplots(5,5,figsize=(3,3))
# for j in range(5):
#     for k in range(5):
#         i = np.random.choice(range(len(x)))
#         axes1[j][k].set_axis_off()
#         axes1[j][k].imshow(x[i:i+1][0])

# Normalize data
x = x / 256.

from models.mnist import mnist_model

m = mnist_model(i.training_dataset.keras_input_shape,
  epochs=100)


  def __init__(self,
    batch_size = 128,
    num_classes = 6,
    epochs = 100):

# # Shuffle and normalize data
# x, y = shuffle(x, y)
# y = to_categorical(y, 10)
