import sys
sys.path.append("../../common/")
import tools
from dataset import dataset

class instance(object):
  def __init__(self,
    momentum_range=(4001, 5000),
    combine=True,
    balance=True,
    balance_fraction=0.5,
    data_ranges=((0.0, 0.8), (0.8, 0.9), (0.9, 1.0))):
    self.momentum_range = momentum_range
    self.balance = balance
    self.balance_fraction = balance_fraction
    # Load training and validation data
    # Balance training dataset
    print("Momentum range", self.momentum_range[0], "to", self.momentum_range[1])
    self.training_dataset = dataset(datatype="training", momentum_range=momentum_range, \
      balance=balance, balance_fraction=balance_fraction, data_range=data_ranges[0])
    self.validation_dataset = dataset(datatype="validation", momentum_range=momentum_range, \
      data_range=data_ranges[1])
    self.test_dataset = dataset(datatype="test", momentum_range=momentum_range, \
      data_range=data_ranges[2])
    # Print some statistics
    print(self.training_dataset.statistics_string())
    print(self.validation_dataset.statistics_string())
    print(self.test_dataset.statistics_string())
  
  def solve(self, model):
    """Solves the problem instance by applying the model
    passed by parameter.
    """
    model.run(self.training_dataset.keras_dataset["x"], self.training_dataset.keras_dataset["y"], 
      self.validation_dataset.keras_dataset["x"], self.validation_dataset.keras_dataset["y"],
      self.test_dataset.keras_dataset["x"], self.test_dataset.keras_dataset["y"])
    
    print('\nTest loss:', model.score[0])
    print('Test accuracy:', model.score[1])
    print('\n' + model.efficiencies_string())
    print(model.identification_string())

    # Save the generated model
    model.save(tools.keras_models_containing_folder(momentum_range=self.momentum_range),
      self.training_dataset,
      self.validation_dataset,
      self.test_dataset,
      self.balance,
      self.balance_fraction
    )
