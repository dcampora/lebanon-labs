#!/usr/bin/python3

import sys
sys.path.append("../../common/")
import tools
from instance import instance
from models.mnist import mnist_model

for model in [mnist_model]: # mnist_model, inception_model
  for mrange in tools.make_momentum_ranges():
    i = instance(momentum_range=mrange, balance=False)
    m = model(i.training_dataset.keras_input_shape, epochs=100)
    i.solve(m)
