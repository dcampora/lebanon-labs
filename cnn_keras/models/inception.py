from models.model import keras_model
import keras
from keras.models import Model
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import Input

class inception_model(keras_model):
  def __init__(self,
    input_shape,
    batch_size = 128,
    num_classes = 6,
    epochs = 100):
    super(inception_model, self).__init__(batch_size, num_classes, epochs)

    input_img = Input(shape = input_shape)
    tower_1 = Conv2D(64, (1,1), padding='same', activation='relu')(input_img)
    tower_1 = Conv2D(64, (3,3), padding='same', activation='relu')(tower_1)

    tower_2 = Conv2D(64, (1,1), padding='same', activation='relu')(input_img)
    tower_2 = Conv2D(64, (5,5), padding='same', activation='relu')(tower_2)

    tower_3 = MaxPooling2D((3,3), strides=(1,1), padding='same')(input_img)
    tower_3 = Conv2D(64, (1,1), padding='same', activation='relu')(tower_3)

    output = keras.layers.concatenate([tower_1, tower_2, tower_3], axis = 3)
    output = Flatten()(output)
    output = Dense(num_classes, activation='softmax')(output)
    
    self.model = Model(inputs=input_img, outputs=output)

    self.model.compile(loss=keras.losses.categorical_crossentropy,
                  optimizer=keras.optimizers.Adadelta(),
                  metrics=['accuracy'])

  def __repr__(self):
    return "inception"
