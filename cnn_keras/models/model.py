from datetime import datetime
import os
import numpy as np
import tools

class keras_model(object):
  def __init__(self,
    batch_size = 128,
    num_classes = 6,
    epochs = 100):
    self.batch_size = batch_size
    self.num_classes = num_classes
    self.epochs = epochs
    self.model = None

  def run(self, x_train, y_train, x_validation, y_validation, x_test, y_test):
    self.model_history = self.model.fit(
      x_train,
      y_train,
      batch_size=self.batch_size,
      epochs=self.epochs,
      verbose=1,
      validation_data=(x_validation, y_validation))
    self.score = self.model.evaluate(x_test, y_test, verbose=0)
    # # Find reconstruction accuracy and loss for each particle type
    # self.efficiencies = {}
    # for k, v in iter(tools.classification.items()):
    #   self.efficiencies[k] = {}
    #   particles = [a for a in zip(x_test, y_test) if a[1][v]==1]
    #   self.efficiencies[k]["number_of_particles"] = len(particles)
    #   if len(particles) == 0:
    #     self.efficiencies[k]["accuracy"] = 1.
    #     self.efficiencies[k]["loss"] = 0.
    #   else:
    #     score = self.model.evaluate(np.array([a[0] for a in particles]), np.array([a[1] for a in particles]))
    #     self.efficiencies[k]["accuracy"] = score[1]
    #     self.efficiencies[k]["loss"] = score[0]
    
    # # Find rate and misprediction rate for each particle type
    # self.predicted_classes = {}
    # for k0, v0 in iter(tools.classification.items()):
    #   self.predicted_classes[v0] = {}
    #   for k1, v1 in iter(tools.classification.items()):
    #     self.predicted_classes[v0][v1] = 0
    
    # # Getting the predicted_classes with a version that works for
    # # any kind of model (and not only Sequential)
    # # Just for Sequential: predicted_classes = self.model.predict_classes(x_test)
    # y_proba = self.model.predict(x_test)
    # predicted_classes = y_proba.argmax(axis=-1)
    # for i in range(len(predicted_classes)):
    #   predicted = predicted_classes[i]
    #   truth = list(y_test[i]).index(1.)
    #   self.predicted_classes[truth][predicted] += 1
    # # Keep also appearing particles in order to print later
    # self.appearing_particles = []
    # for k, v in iter(self.efficiencies.items()):
    #   if v["number_of_particles"] > 0:
    #     self.appearing_particles.append(k)
    # self.appearing_particles.sort()

  def save(self, folder, learning_dataset, validation_dataset, test_dataset, balance=False, balance_fraction=0.0):
    """Saves the current model to the specified folder."""
    if self.model == None:
      print("Warning: The model is empty!")
    else:
      if not os.path.isdir(folder):
        tools.make_dir(folder)
      strtime = datetime.now().strftime("%y%m%d_%H%M")
      json_filename = folder + strtime + "_model.json"
      weights_filename = folder + strtime + "_weights.h5"
      statistics_filename = folder + strtime + "_statistics.txt"
      # TODO: For the moment this doesn't seem to work for multi_gpu
      # model_json = self.model.to_json()
      # with open(json_filename, "w") as json_file:
      #   json_file.write(model_json)
      with open(statistics_filename, "w") as statistics_file:
        statistics_file.write("Using " + self.__repr__() + " model\n batch size " + str(self.batch_size) + "\n"
          + " epochs " + str(self.epochs) + "\n"
          + " balanced data " + str(balance) + ((" " + str(balance_fraction) if balance else ""))
          + "\n\n")
        import sys
        prev_stdout = sys.stdout
        sys.stdout = statistics_file
        self.model.summary()
        sys.stdout = prev_stdout
        statistics_file.write("\n" + learning_dataset.statistics_string() + "\n")
        statistics_file.write(validation_dataset.statistics_string() + "\n")
        statistics_file.write(test_dataset.statistics_string() + "\n")
        statistics_file.write("Test accuracy: %.6f\nTest loss: %.6f\n\n" % (self.score[1], self.score[0]))
        statistics_file.write(self.efficiencies_string() + "\n")
      self.model.save_weights(weights_filename)
      print("Saved model to disk")

  def efficiencies_string(self):
    output_str = "Particle type\t# particles\tAccuracy\tLoss\n" + "".join(["-"]*56) + "\n"
    # for k in self.appearing_particles:
    #   e = self.efficiencies[k]
    #   if e["number_of_particles"] > 0:
    #     tab = "\t\t" if k != "electron" else "\t"
    #     output_str += k + tab \
    #       + str(e["number_of_particles"]) + "\t\t" \
    #       + ("%.6f\t" % e["accuracy"]) \
    #       + ("%.6f\n" % e["loss"])
    return output_str

  def __repr__(self):
    return "base"
