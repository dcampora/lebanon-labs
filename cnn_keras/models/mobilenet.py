from models.model import keras_model
import keras
from keras.models import Model
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import Input
from keras.applications import mobilenet

class mobilenet_model(keras_model):
  def __init__(self,
    input_shape,
    batch_size = 128,
    num_classes = 6,
    epochs = 100):
    super(mobilenet_model, self).__init__(batch_size, num_classes, epochs)

    self.model = mobilenet.MobileNet(
      input_shape=input_shape,
      alpha=1.0,
      depth_multiplier=1,
      dropout=1e-3,
      weights=None,
      include_top=True,
      input_tensor=None,
      pooling=None,
      classes=num_classes)

    self.model.compile(loss=keras.losses.categorical_crossentropy,
                  optimizer='rmsprop',
                  metrics=['accuracy'])

  def __repr__(self):
    return "mobilenet"
