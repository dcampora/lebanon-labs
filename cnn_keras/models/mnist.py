from models.model import keras_model
import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D

class mnist_model(keras_model):
  def __init__(self,
    input_shape,
    batch_size = 128,
    num_classes = 6,
    epochs = 100):
    super(mnist_model, self).__init__(batch_size, num_classes, epochs)

    self.model = Sequential()
    self.model.add(Conv2D(32,
                          kernel_size=(3, 3),
                          activation='relu',
                          input_shape=input_shape))
    self.model.add(Conv2D(64,
                          kernel_size=(3, 3),
                          activation='relu'))
    self.model.add(MaxPooling2D(pool_size=(2, 2)))
    self.model.add(Dropout(0.25))
    self.model.add(Flatten())
    self.model.add(Dense(128,
                         activation='relu'))
    self.model.add(Dropout(0.5))
    self.model.add(Dense(num_classes,
                         activation='softmax'))

    self.model.compile(loss=keras.losses.categorical_crossentropy,
                  optimizer=keras.optimizers.Adadelta(),
                  metrics=['accuracy'])

  def __repr__(self):
    return "mnist"
