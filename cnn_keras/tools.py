import os

def make_dir(dir):
  try:
    os.makedirs(dir)
  except:
    print("Warning:", dir, "exists. Files may be overwritten.")
