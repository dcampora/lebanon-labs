import sys
sys.path.append("../../common/")
from keras import utils as keras_utils, backend as K
from collections import OrderedDict
import tools
import png
import os
import pickle
import math
import numpy as np

class dataset(object):
  def __init__(self,
    datatype="training",
    momentum_range=(4001, 5000),
    base_containing_folder="/afs/cern.ch/user/d/dcampora/data/rich_cnn_20170301/",
    dopickle=False,
    balance=False,
    balance_fraction=0.9,
    data_range=(0.0, 0.8),
    generate_ambiguous=False):
    self.__datatype = datatype
    self.__momentum_range = momentum_range
    self.__dopickle = True
    self.__data_range = data_range
    self.__generate_ambiguous = generate_ambiguous
    self.dataset = {"x": [], "y": []}

    # Make containing folder with some hard-coded knowledge
    self.__containing_folder = base_containing_folder + str(momentum_range[0]) + "_" + str(momentum_range[1]) + "/"

    # Load the dataset
    self.__load_from_binary(self.__containing_folder)

    # Balance dataset if requested
    if balance:
      self.__balance(balance_fraction)

    # Convert to arrays
    self.__convert_to_array()
    self.__generate_keras_dataset()

  def get_statistics(self, classification=tools.classification):
    pid_percentage = OrderedDict({0: 0, 1: 0, 2: 0, 3: 0, 4: 0})
    for k, v in iter(classification.items()):
      if len(self.dataset["y"]) > 0:
        pid_percentage[v] = len([a for a in self.dataset["y"] if a==v]) / len(self.dataset["y"])
      else:
        pid_percentage[v] = 1.0
    return pid_percentage

  def statistics_string(self, classification=tools.classification):
    output_str = self.__datatype + " dataset:\n " + str(len(self.dataset["y"])) + " particles\n"
    statistics = self.get_statistics(classification)
    for k, v in iter(classification.items()):
      try:
        percentage = 100 * statistics[v]
        output_str += " " + k + (" : %.4f %%" % percentage) + "\n"
      except:
        pass
    return output_str

  def __load_from_binary(self, containing_folder):
    folderlist = os.listdir(containing_folder)
    # Check how many particles do we have
    self.__number_of_particles = 0
    for filename in [a for a in folderlist if "y" in a]:
      self.__number_of_particles += os.stat(containing_folder + filename).st_size

    self.__particle_int_range = [int(math.floor(self.__data_range[0] * self.__number_of_particles)),
      int(math.floor(self.__data_range[1] * self.__number_of_particles))]
    if self.__data_range[1] == 1.0:
      self.__particle_int_range[1] -= 1

    print("Fetching", self.__datatype, "range", self.__particle_int_range)
    start, end = self.__particle_int_range[0], self.__particle_int_range[1]

    counter = 0
    for (xfilename, yfilename) in zip(sorted([a for a in folderlist if "x" in a]),\
                                      sorted([a for a in folderlist if "y" in a])):
      f = open(containing_folder + xfilename, "rb")
      xparticles = f.read()
      f.close()
      f = open(containing_folder + yfilename, "rb")
      yparticles = f.read()
      f.close()
      number_of_particles = os.stat(containing_folder + yfilename).st_size

      for i in range(number_of_particles):
        particle_id = counter + i
        if particle_id >= start and particle_id < end:
          pids = [a for a, b in iter(tools.classification.items()) if (1 << b) & yparticles[i]]
          if not self.__generate_ambiguous and len(pids) >= 2:
            pids = []

          for pid in pids:
            self.dataset["y"].append(tools.classification[pid])
            image_2d = []
            for row_index in range(32):
              row = []
              for j in range(4):
                try:
                  byte = xparticles[(4*32)*i + 4*row_index + j]
                  for bit in range(7, -1, -1):
                    row.append((byte >> bit) & 0x01)
                except:
                  print(particle_id, row_index, j)
                  print((4*32)*i + 32*row_index + j)
                  raise
              image_2d.append(row)
            self.dataset["x"].append(np.vstack(image_2d))

      counter += number_of_particles

  def __balance(self, fraction):
    """Balances the dataset by removing the most populated particle type.
    It makes the highest equal to the second-highest. The multiplier
    allows to remove a fraction instead of all. ie.

    nb_elements_most_popular -= fraction * (nb_elements_most_popular - second_most_popular)
    """
    pid_percentage = self.get_statistics()
    most_popular = max(iter(pid_percentage.items()), key=lambda x: x[1])
    del pid_percentage[most_popular[0]]
    second_most_popular = max(iter(pid_percentage.items()), key=lambda x: x[1])
    elements_to_delete = math.floor(len(self.dataset["y"]) * (most_popular[1] - second_most_popular[1]) * fraction)
    
    deleted = 0
    for i in range(len(self.dataset["y"])-1, -1, -1):
      if deleted >= elements_to_delete:
        break
      if self.dataset["y"][i] == most_popular[0]:
        del self.dataset["y"][i]
        del self.dataset["x"][i]
        deleted += 1
    self.__number_of_particles = len(self.dataset["y"])

  def __convert_to_array(self):
    self.dataset["x"] = np.array(self.dataset["x"])
    self.dataset["y"] = np.array(self.dataset["y"])

  def __generate_keras_dataset(self, rows=32, cols=32, num_classes=len(tools.classification)):
    self.keras_dataset = {}
    if K.image_data_format() == 'channels_first':
      self.keras_dataset["x"] = self.dataset["x"].reshape(self.dataset["x"].shape[0], 1, rows, cols)
      self.keras_input_shape = (1, rows, cols)
    else:
      self.keras_dataset["x"] = self.dataset["x"].reshape(self.dataset["x"].shape[0], rows, cols, 1)
      self.keras_input_shape = (rows, cols, 1)
    self.keras_dataset["x"] = self.keras_dataset["x"].astype('float32')
    self.keras_dataset["y"] = keras_utils.to_categorical(self.dataset["y"], num_classes)
