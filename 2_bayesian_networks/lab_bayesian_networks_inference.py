#============================================================
# @brief  Lab - Inference in Bayesian networks
# @author D H Campora Perez
#         translated from IATI lab,
#         Dpto. de Ciencias de la Computacion e I. A., USE
#============================================================

# Start random with a seed, for reproducible results
import random
random.seed(0)

# In this lab, we will implement probabilistic inference in
# Bayesian networks, using sampling techniques.

### Data structures for representing Bayesian networks.

# Let's use a fixed data structure to represent Bayesian networks.
# For instance, the variable alarm_network encodes the Bayesian
# network representation of the alarm example of class:

alarm_network=[{"robbery":[True,False],
             "earthquake":[True,False],
             "alarm":[True,False],
             "john_calls":[True,False],
             "mary_calls":[True,False]},
             
             {"robbery":[],
              "earthquake":[],
              "alarm":["robbery","earthquake"],
              "john_calls":["alarm"],
              "mary_calls":["alarm"]},
              
             {"robbery":{():[0.001,0.999]},
              "earthquake":{():[0.002,0.998]},
              "alarm":{(True,True):[0.95,0.05],
                       (True,False):[0.94,0.06],
                       (False,True):[0.29,0.71],
                       (False,False):[0.001,0.999]},
              "john_calls":{(True,):[0.9,0.1],
                            (False,):[0.05,0.95]},
              "mary_calls":{(True,):[0.7,0.3],
                            (False,):[0.01,0.99]}}]

# A Bayesian network will be represented in these exercises as a list
# of three elements:
#
# 1. Random variables and their possible values: A dictionary mapping
#    each variable name to a list of its possible values.
#
# 2. Parents of each variable in the network: A dictionary that maps
#    each variable name a list of its parents.
#
# 3. Probabilities for each node: A dictionary mapping each variable name
#    to its probability table. At the same time, the table associated 
#    to each variable X is a dictionary that maps each combination of its
#    parents values to the conditioned probability distribution of the variable X
#    given those specific values.
#
#    For instance, if the table "alarm" contains a value:
#
#    (True, False): [0.94, 0.06]
#    
#    it means:
#
#    P(alarm=True | robbery=True, earthquake=False) = 0.94
#    P(alarm=False | robbery=True, earthquake=False) = 0.06
#
#    Note the implicit order of the values of a variable, and that of the parents,
#    is the same as the one defined in their corresponding dictionaries.
#
# Here are two more probability tables shown in class:

ictus_network=[{"athletic":[True,False],
              "healthy_nutrition":[True,False],
              "hypertensive":[True,False],
              "smoker":[True,False],
              "ictus":[True,False]},
              
              {"athletic":[],
               "healthy_nutrition":[],
               "hypertensive":["athletic","healthy_nutrition"],
               "smoker":[],
               "ictus":["hypertensive","smoker"]},
               
               {"athletic":{():[0.1,0.9]},
                "healthy_nutrition":{():[0.4,0.6]},
                "hypertensive":{(True,True):[0.01,0.99],
                                (True,False):[0.25,0.75],
                                (False,True):[0.2,0.8],
                                (False,False):[0.7,0.3]},
                "smoker":{():[0.4,0.6]},
                "ictus":{(True,True):[0.8,0.2],
                         (True,False):[0.7,0.3],
                         (False,True):[0.6,0.4],
                         (False,False):[0.3,0.7]}}]

sprinklers_network=[{"wet_grass":[True,False],
               "rain":[True,False],
               "overcast":[True,False],
               "sprinklers":[True,False]},

               {"overcast":[],
               "sprinklers":["overcast"],
               "rain":["overcast"],
               "wet_grass":["sprinklers","rain"]},

               {"overcast":{():[0.5,0.5]},
                "sprinklers":{(True,):[0.1,0.9],
                              (False,):[0.5,0.5]},
                "rain":{(True,):[0.8,0.2],
                        (False,):[0.2,0.8]},
                "wet_grass":{(True,True):[0.99,0.01],
                             (True,False):[0.9,0.1],
                             (False,True):[0.9,0.1],
                             (False,False):[0.0,1.0]}}]

### Auxiliary functions

#### Exercise 1

# Define the function "compatible_order(net)", that receives a Bayesian network
# and returns a topological ordering of the graph encoded in the network.
# In other words, it returns a sequence (list) of all the variables in the network
# fulfilling that for every variable X that is a parent of another variable Y,
# X precedes Y in the sequence.

# Examples (other possibilities exist):

# >>> compatible_order(alarm_network)
# ['earthquake', 'robbery', 'alarm', 'john_calls', 'mary_calls']
# >>> compatible_order(ictus_network)
# ['athletic', 'smoker', 'healthy_nutrition', 'hypertensive', 'ictus']
# >>> compatible_order(sprinklers_network)
# ['overcast', 'sprinklers', 'rain', 'wet_grass']

def compatible_order(net):
  variables, parents, probabilities = net[0], net[1], net[2]
  ordered_parents = []
  # Iterate until all parents are in ordered_parents
  while len(ordered_parents) < len(parents):
    for node, node_parents in iter(parents.items()):
      # Only add a node to ordered_parents if it was not added already
      # and if all parents of that node were already added
      if node not in ordered_parents:
        if len([a for a in node_parents if a in ordered_parents]) == len(node_parents):
          ordered_parents.append(node)
  return ordered_parents

print(compatible_order(alarm_network))
print(compatible_order(ictus_network))
print(compatible_order(sprinklers_network))

#### Exercise 2

# Define the function "sampling(values, probabilities)". It should receive as input
# a list of n values of a random variable and a probability distribution
# (a list of n probabilities). It should return one of the values in the list randomly.
# The probability of it returning each of these values must correspond with
# the probability of the distribution.
#
# Examples (take into account you may randomly get different values):
#
# >>> sampling(["v1","v2","v3"],[0.2,0.6,0.2])
# 'v3'
# >>> sampling(["v1","v2","v3"],[0.2,0.6,0.2])
# 'v2'
# >>> sampling(["v1","v2","v3"],[0.2,0.6,0.2])
# 'v2'
# >>> sampling(["v1","v2","v3"],[0.2,0.6,0.2])
# 'v2'
# >>> sampling(["v1","v2","v3"],[0.2,0.6,0.2])
# 'v2'
# >>> sampling(["v1","v2","v3"],[0.2,0.6,0.2])
# 'v2'
# >>> sampling(["v1","v2","v3"],[0.2,0.6,0.2])
# 'v2'
#
# Verify you indeed get the expected distribution by generating
# a large amount of events with the intended probability.

# Note: Use this random function to retrieve a value in [0.0, 1.0]
def generate_random_value():
  return random.randint(1, 10000) / 10000.0

def sampling(values, probabilities):
  if not (sum(probabilities) > 0.9999999 and sum(probabilities) < 1.00000001):
    raise "The addition of the probabilities is not 1.0"
  rando = generate_random_value()
  range_start = 0.0
  for value, range_sum in zip(values, probabilities):
    range_end = range_start + range_sum
    if rando >= range_start and rando < range_end:
      return value
    range_start += range_sum
  return values[-1]

print(sampling(["v1","v2","v3"], [0.2,0.6,0.2]))
print(sampling(["v1","v2","v3"], [0.2,0.6,0.2]))
print(sampling(["v1","v2","v3"], [0.2,0.6,0.2]))
print(sampling(["v1","v2","v3"], [0.2,0.6,0.2]))
print(sampling(["v1","v2","v3"], [0.2,0.6,0.2]))
print(sampling(["v1","v2","v3"], [0.2,0.6,0.2]))
print(sampling(["v1","v2","v3"], [0.2,0.6,0.2]))

#### Exercise 3

# Define the function "a_priori_sampling(net)". It should generate a full random event,
# following the distribution the Bayesian network in the input defines.
# (You may look at theory slides 95-97).
#
# Examples (take into account results are random):

# >>> a_priori_sampling(alarm_network)
# {'john_calls': False, 'earthquake': False, 'robbery': False, 'alarm': False, 'mary_calls': False}
# >>> a_priori_sampling(alarm_network)
# {'john_calls': False, 'earthquake': False, 'robbery': False, 'alarm': False, 'mary_calls': False}
# >>> a_priori_sampling(sprinklers_network)
# {'overcast': True, 'rain': True, 'wet_grass': False, 'sprinklers': False}
# >>> a_priori_sampling(sprinklers_network)
# {'overcast': False, 'rain': False, 'wet_grass': False, 'sprinklers': False}
# >>> a_priori_sampling(ictus_network)
# {'healthy_nutrition': False, 'smoker': True, 'ictus': True, 
#  'hypertensive': True, 'athletic': False}
# >>> a_priori_sampling(ictus_network)
# {'healthy_nutrition': False, 'smoker': True, 'ictus': True, 
#  'hypertensive': False, 'athletic': False}

def truth_assignment_tuple(parents, assignment, node):
  parents_of_node = parents[node]
  return tuple([assignment[a] for a in parents_of_node])

def a_priori_sampling(net):
  variables, parents, probabilities = net[0], net[1], net[2]
  nodes_order = compatible_order(net)
  truth_assignment = {}
  for node in nodes_order:
    truth_tuple = truth_assignment_tuple(parents, truth_assignment, node)
    probabilities_node = probabilities[node][truth_tuple]
    node_assignment = sampling(variables[node], probabilities_node)
    truth_assignment[node] = node_assignment
  return truth_assignment

print(a_priori_sampling(alarm_network))
print(a_priori_sampling(alarm_network))
print(a_priori_sampling(sprinklers_network))
print(a_priori_sampling(sprinklers_network))
print(a_priori_sampling(ictus_network))
print(a_priori_sampling(ictus_network))

### Approximate inference

#### Exercise 4

# Define the function "rejection_sampling(var, observed, net, N)".
# It makes an approximation of the probability P(var | observed), with respect to
# a given Bayesian network, using the rejection sampling method (slides 98-99).
# N indicates the number of samples that should be generated.
#
# Experiment with probabilities of events you know the answer to (examples below).
# Change the value N to see how a better number of samples accounts for a better approximation.

# Examples:

# >>> rejection_sampling("robbery",{"john_calls":True,"mary_calls":True},alarm_network,100000)
# {True: 0.32085561497326204, False: 0.679144385026738}

# Note: The exact probability is <False:0.71583,True:0.28417> (see slides)

# >>> rejection_sampling("smoker",{"ictus":True,"athletic":False},ictus_network,100000)
# {True: 0.4842513636014443, False: 0.5157486363985557}

# Note: The exact probability is <False:0.52,True:0.48> (see slides)

# You may use the following normalization function

def normalize(d):
    _sum = sum(d.values())
    return {a:d[a] / float(_sum) for a in d}

# Examples:
#
# >>> normalize({"v1":0.21,"v2":0.11,"v3":0.37})
# {'v1': 0.30434782608695654, 'v2': 0.15942028985507248, 'v3': 0.5362318840579711}
# >>> normalize({True:0.012,False:0.008})
# {False: 0.4, True: 0.6}

print(normalize({"v1":0.21,"v2":0.11,"v3":0.37}))
print(normalize({True:0.012,False:0.008}))

def rejection_sampling(var, observed, net, N):
  variables, parents, probabilities = net[0], net[1], net[2]
  NX = {a:0 for a in variables[var]}
  for i in range(N):
    ap_sampling = a_priori_sampling(net)
    compliant = [ap_sampling[k]==v for k, v in iter(observed.items())]
    if False not in compliant:
      NX[ap_sampling[var]] += 1
  return normalize(NX)

print(rejection_sampling("robbery",{"john_calls":True,"mary_calls":True},alarm_network,100000))
print(rejection_sampling("smoker",{"ictus":True,"athletic":False},ictus_network,100000))

#### Exercise 5

# The main problem of rejection sampling is that many of the generated samples are rejected
# if they are not compatible with the observed values.
#
# Test this experimentally. Just modify the rejection_sampling function of the previous
# exercise, so that it also prints out the number of rejected samples.

# Example:

# >>> rejection_sampling_bis("robbery",{"john_calls":True,"mary_calls":True},alarm_network,10000)
# Rejected 9978 samples out of 10000
# {True: 0.22727272727272727, False: 0.7727272727272727}

def rejection_sampling_bis(var, observed, net, N):
  variables, parents, probabilities = net[0], net[1], net[2]
  NX = {a:0 for a in variables[var]}
  rejected_samples = 0
  for i in range(N):
    ap_sampling = a_priori_sampling(net)
    compliant = [ap_sampling[k]==v for k, v in iter(observed.items())]
    if False not in compliant:
      NX[ap_sampling[var]] += 1
    else:
      rejected_samples += 1
  print("Rejected", rejected_samples, "samples out of", N)
  return normalize(NX)

print(rejection_sampling_bis("robbery",{"john_calls":True,"mary_calls":True},alarm_network,10000))

#### Exercise 6

# In order to solve the existing problem of rejection sampling, the
# likelihood weighting algorithm is proposed. It generates samples
# that are compatible with the observations, forcing certain variables
# to take determined values. As a consequence, each sample is generated with
# an associated "weight" = the probability of the sample happening randomly.
#
# The likelihood weighting algorithm allow us to solve approximate inference
# for small sized-networks. In our case, we will use it with the following network,
# taken from the "Car Starting Problem":

car_starting_network=[{"alternator_OK":[True,False],
                     "charging_system_OK":[True,False],  
                     "battery_age":["new", "old", "very_old"],  
                     "battery_voltage":["strong", "weak", "dead"], 
                     "main_fuse_OK":[True,False],
                     "distributer_OK":[True,False],
                     "voltage_at_plug":["strong", "weak", "dead"],
                     "starter_motor_OK":[True,False],
                     "starter_system_OK":[True,False],
                     "headlights":["bright", "dim", "off"],
                     "spark_plugs":["okay", "too_wide", "fouled"],
                     "car_cranks":[True,False],
                     "spark_timing":["good", "bad", "very_bad"],
                     "fuel_system_OK":[True,False],
                     "air_filter_clean":[True,False],
                     "air_system_OK":[True,False],
                     "car_starts":[True,False],
                     "spark_quality":["good", "bad", "very_bad"],
                     "spark_adequate":[True,False]},
                     
         {"alternator_OK":[],
         "charging_system_OK":["alternator_OK"], 
                   "battery_age":[], 
                   "battery_voltage":["charging_system_OK", "battery_age"], 
                   "main_fuse_OK":[],
                   "distributer_OK":[],
                   "voltage_at_plug":["battery_voltage", "main_fuse_OK", "distributer_OK"],
                   "starter_motor_OK":[],
                   "starter_system_OK":["battery_voltage", "main_fuse_OK", "starter_motor_OK"],
                   "headlights":["voltage_at_plug"],
                   "spark_plugs":[],
                   "car_cranks":["starter_system_OK"],
                   "spark_timing":["distributer_OK"],
                   "fuel_system_OK":[],
                   "air_filter_clean":[],
                   "air_system_OK":["air_filter_clean"],
                   "car_starts":["car_cranks", "fuel_system_OK", 
                                 "air_system_OK", "spark_adequate"],
                   "spark_quality":["voltage_at_plug", "spark_plugs"],
                   "spark_adequate":["spark_timing", "spark_quality"]},


         {"alternator_OK":{():[0.9997,0.0003]},
         "charging_system_OK":{(True,):[0.995, 0.005],
                               (False,):[0.0, 1.0]}, 
                   "battery_age":{():[0.4, 0.4, 0.2]}, 
                   "battery_voltage":{(True,"new"):[0.999, 0.0008, 0.0002],
                                      (True,"old"):[0.99, 0.008, 0.002],
                                      (True,"very_old"):[0.6, 0.3, 0.1],         
                                      (False,"new"):[0.8, 0.15, 0.05],
                                      (False,"old"):[0.05, 0.3, 0.65],
                                      (False,"very_old"):[0.002, 0.1, 0.898]}, 
                   "main_fuse_OK":{():[0.999, 0.001]}, 
                   "distributer_OK":{():[0.99, 0.01]},
                   "voltage_at_plug":{("strong", True, True):[0.98, 0.015, 0.005],
                                      ("strong", True, False):[0.0, 0.0, 1.0],
                                      ("strong", False, True):[0.0, 0.0, 1.0],
                                      ("strong", False, False):[0.0, 0.0, 1.0],
                                      ("weak", True, True):[0.1, 0.8, 0.1],
                                      ("weak", True, False):[0.0, 0.0, 1.0],
                                      ("weak", False, True):[0.0, 0.0, 1.0],
                                      ("weak", False, False):[0.0, 0.0, 1.0],
                                      ("dead", True, True):[0.0, 0.0, 1.0],
                                      ("dead", True, False):[0.0, 0.0, 1.0],
                                      ("dead", False, True):[0.0, 0.0, 1.0],
                                      ("dead", False, False):[0.0, 0.0, 1.0]},
                   "starter_motor_OK":{():[0.992, 0.008]},
                   "starter_system_OK":{("strong", True, True):[ 0.998, 0.002],
                                        ("strong", True, False):[ 0.0, 1.0],
                                        ("strong", False, True):[ 0.0, 1.0],
                                        ("strong", False, False):[ 0.0, 1.0],
                                        ("weak", True, True):[ 0.72, 0.28],
                                        ("weak", True, False):[ 0.0, 1.0],
                                        ("weak", False, True):[ 0.0, 1.0],
                                        ("weak", False, False):[ 0.0, 1.0],
                                        ("dead", True, True):[ 0.0, 1.0],
                                        ("dead", True, False):[ 0.0, 1.0],
                                        ("dead", False, True):[ 0.0, 1.0],
                                        ("dead", False, False):[ 0.0, 1.0]},
                   "headlights":{("strong",):[0.98, 0.015, 0.005],
                                 ("weak",):[0.05, 0.9, 0.05],
                                 ("dead",):[0.0, 0.0, 1.0]},
                   "spark_plugs":{():[0.99, 0.003, 0.007]},
                   "car_cranks":{(True,):[0.98, 0.02],
                                         (False,):[0.0, 1.0]},
                   "spark_timing":{(True,):[0.97, 0.02, 0.01],
                                              (False,):[0.2, 0.3, 0.5]},
                   "fuel_system_OK":{():[0.9, 0.1]},
                   "air_filter_clean":{():[0.9, 0.1]},
                   "air_system_OK":{(True,):[0.9, 0.1],
                                             (False,):[0.3, 0.7]},
                   "car_starts":{ (True, True, True, True):[ 1.0, 0.0],
                                  (True, True, True, False):[ 0.0, 1.0],
                                  (True, True, False, True):[ 0.0, 1.0],
                                  (True, True, False, False):[ 0.0, 1.0],
                                  (True, False, True, True):[ 0.0, 1.0],
                                  (True, False, True, False):[ 0.0, 1.0],
                                  (True, False, False, True):[ 0.0, 1.0],
                                  (True, False, False, False):[ 0.0, 1.0],
                                  (False, True, True, True):[ 0.0, 1.0],
                                  (False, True, True, False):[ 0.0, 1.0],
                                  (False, True, False, True):[ 0.0, 1.0],
                                  (False, True, False, False):[ 0.0, 1.0],
                                  (False, False, True, True):[ 0.0, 1.0],
                                  (False, False, True, False):[ 0.0, 1.0],
                                  (False, False, False, True):[ 0.0, 1.0],
                                  (False, False, False, False):[ 0.0, 1.0]},
                   "spark_quality":{("strong", "okay"):[ 1.0, 0.0, 0.0],
                                    ("strong", "too_wide"):[ 0.0, 1.0, 0.0],
                                    ("strong", "fouled"):[ 0.0, 0.0, 1.0],
                                    ("weak", "okay"):[ 0.0, 1.0, 0.0],
                                    ("weak", "too_wide"):[ 0.0, 0.5, 0.5],
                                    ("weak", "fouled"):[ 0.0, 0.2, 0.8],
                                    ("dead", "okay"):[ 0.0, 0.0, 1.0],
                                    ("dead", "too_wide"):[ 0.0, 0.0, 1.0],
                                    ("dead", "fouled"):[ 0.0, 0.0, 1.0]},
                   "spark_adequate":{("good", "good"):[ 0.99, 0.01],
                                     ("good", "bad"):[ 0.5, 0.5],
                                     ("good", "very_bad"):[ 0.1, 0.9],
                                     ("bad", "good"):[ 0.5, 0.5],
                                     ("bad", "bad"):[ 0.05, 0.95],
                                     ("bad", "very_bad"):[ 0.01, 0.99],
                                     ("very_bad", "good"):[ 0.1, 0.9],
                                     ("very_bad", "bad"):[ 0.01, 0.99],
                                     ("very_bad", "very_bad"):[ 0.0, 1.0]}}]

# Define the function "likelihood_weighting(var, observed, net, N)" that implements
# an approximate calculation of the probability P(var | observed), within the
# Bayesian network given as an input. It should use the likelihood weighting method
# (slides 100-103). N indicates the number of samples to be done.

# Examples:

# >>> likelihood_weighting("robbery",{"john_calls":True,"mary_calls":True},alarm_network,100000)
# {True: 0.2631912671574272, False: 0.7368087328425729}
# 
# Note: The exact probability is <True:0.28417, False:0.71583>

# >>> likelihood_weighting("fuel_system_OK",
#                                   {"battery_age":"old",
#                                    "alternator_OK":True, 
#                                    "air_filter_clean":False,
#                                    "car_starts":False},
#                                   car_starting_network,1000)
# {True: 0.8537549407114629, False: 0.14624505928853715}
#
# Note: The exact probability is <True:0.86773, False:0.13227>

def likelihood_weighting(var, observed, net, N):
  variables, parents, probabilities = net[0], net[1], net[2]
  variables_in_order = compatible_order(net)
  counts = {variable: 0.0 for variable in variables[var]}

  for i in range(N):
    sample = {}
    weight = 1.0
    for j in variables_in_order:
      truth_tuple = truth_assignment_tuple(parents, sample, j)
      probabilities_node = probabilities[j][truth_tuple]
      if j in observed.keys():
        observed_index = variables[j].index(observed[j])
        weight *= probabilities_node[observed_index]
        sample[j] = observed[j]
        # print("Variable", j, "with sampling", sample, "updated weight:", weight)
      else:
        sample[j] = sampling(variables[j], probabilities_node)
        # print("Variable", j, "with sampling", sample)
    v = sample[var]
    counts[v] += weight
    # print("Final sample:", sample, "final weight:", weight, "updated counts:", counts)
  return normalize(counts)


print(likelihood_weighting("robbery",
                           {"john_calls": True, "mary_calls": True},
                           alarm_network,
                           100000))

print(likelihood_weighting("fuel_system_OK",
                           {"battery_age": "old",
                            "alternator_OK": True, 
                            "air_filter_clean": False,
                            "car_starts": False},
                           car_starting_network,
                           1000))
