#============================================================
# @brief  Lab - Inductive learning
# @author D H Campora Perez
#         translated from IATI lab,
#         dpto. de Ciencias de la Computacion e I. A., USE
#============================================================

import math

# In the following exercises we will implement a simplified version of the ID3 algorithm.
# The last exercise is about relative frequency, as used in the Sequential covering algorithm.
# 
# In the following exercises:
# * Classification values are "yes" and "no".
# * The classification attribute is always the last in the list of attributes.
# * The training sets do not contain any errors.
# 
# We represent a training set as a list of two lists. The first list
# is the attribute list, and the second one is the example list.
# An example is a list of values.

TS_1 = [['age', 'astigmatism', 'tear_rate', 'lenses'], 
        [['young'         , 'no' , 'reduced'  , 'no'],
         ['young'         , 'no' , 'normal'   , 'no'],
         ['young'         , 'yes', 'increased', 'yes'],
         ['young'         , 'yes', 'normal'   , 'yes'],
         ['pre_presbyopia', 'no' , 'reduced'  , 'yes'],
         ['pre_presbyopia', 'no' , 'normal'   , 'no'],
         ['pre_presbyopia', 'yes', 'reduced'  , 'no'],
         ['pre_presbyopia', 'yes', 'increased', 'yes'],
         ['presbyopia'    , 'no' , 'reduced'  , 'no'],
         ['presbyopia'    , 'no' , 'normal'   , 'no'],
         ['presbyopia'    , 'no' , 'increased', 'no'],
         ['presbyopia'    , 'yes', 'reduced'  , 'no'],
         ['presbyopia'    , 'yes', 'normal'   , 'yes']]]

TS_2 = [['sky', 'temperature', 'humidity', 'wind', 'practice_sport'],
        [['sunny', 'high', 'high'  , 'weak'  , 'no'],
         ['sunny', 'high', 'high'  , 'strong', 'no'],
         ['foggy', 'high', 'high'  , 'weak'  , 'yes'],
         ['rainy', 'mild', 'high'  , 'weak'  , 'yes'],
         ['rainy', 'low' , 'normal', 'weak'  , 'yes'],
         ['rainy', 'low' , 'normal', 'strong', 'no'],
         ['foggy', 'low' , 'normal', 'strong', 'yes'],
         ['sunny', 'mild', 'high'  , 'weak'  , 'no'],
         ['sunny', 'low' , 'normal', 'weak'  , 'yes'],
         ['rainy', 'mild', 'normal', 'weak'  , 'yes'],
         ['sunny', 'mild', 'normal', 'strong', 'yes'],
         ['foggy', 'mild', 'high'  , 'strong', 'yes'],
         ['foggy', 'high', 'normal', 'weak'  , 'yes'],
         ['rainy', 'mild', 'high'  , 'strong', 'no']]]  

TS_3 = [['age', 'prescription', 'astigmatism', 'tear_rate', 'lenses'],
        [['young'         ,'myopia'       , 'no' , 'reduced', 'none'],
         ['young'         ,'myopia'       , 'no' , 'normal' , 'soft'],
         ['young'         ,'myopia'       , 'yes', 'reduced', 'none'],
         ['young'         ,'myopia'       , 'yes', 'normal' , 'hard'],
         ['young'         ,'hypermetropia', 'no' , 'reduced', 'none'],
         ['young'         ,'hypermetropia', 'no' , 'normal' , 'soft'],
         ['young'         ,'hypermetropia', 'yes', 'reduced', 'none'],
         ['young'         ,'hypermetropia', 'yes', 'normal' , 'hard'],
         ['pre_presbyopia','myopia'       , 'no' , 'reduced', 'none'],
         ['pre_presbyopia','myopia'       , 'no' , 'normal' , 'soft'],
         ['pre_presbyopia','myopia'       , 'yes', 'reduced', 'none'],
         ['pre_presbyopia','myopia'       , 'yes', 'normal' , 'hard'],
         ['pre_presbyopia','hypermetropia', 'no' , 'reduced', 'none'],
         ['pre_presbyopia','hypermetropia', 'no' , 'normal' , 'soft'],
         ['pre_presbyopia','hypermetropia', 'yes', 'reduced', 'none'],
         ['pre_presbyopia','hypermetropia', 'yes', 'normal' , 'none'],
         ['presbyopia'    ,'myopia'       , 'no' , 'reduced', 'none'],
         ['presbyopia'    ,'myopia'       , 'no' , 'normal' , 'none'],
         ['presbyopia'    ,'myopia'       , 'yes', 'reduced', 'none'],
         ['presbyopia'    ,'myopia'       , 'yes', 'normal' , 'hard'],
         ['presbyopia'    ,'hypermetropia', 'no' , 'reduced', 'none'],
         ['presbyopia'    ,'hypermetropia', 'no' , 'normal' , 'soft'],
         ['presbyopia'    ,'hypermetropia', 'yes', 'reduced', 'none'],
         ['presbyopia'    ,'hypermetropia', 'yes', 'normal' , 'none']]]

###############################################################################
# Exercise 1:
# Define the function entropy(P, N) that receives as input two natural numbers P and N,
# and returns the entropy value for a training set with P positive examples and
# N negative ones.
# 
# Examples:
# >>> entropy(9,5)
#     0.9402859586706309
# >>> entropy(9,0)
#     0
# >>> entropy(0,5)
#     0
###############################################################################

def entropy(P, N):
  if P==0 or N==0:
    return 0
  D = P + N
  return - (P / D) * math.log(P / D, 2) - (N / D) * math.log(N / D, 2)

# print(entropy(9, 5))
# print(entropy(9, 0))
# print(entropy(0, 5))

###############################################################################
# Exercise 2:
# Define the function gain(D, A). It receives a training set D
# and an attribute A and returns its information gain.
#
# Examples:
# >>> gain(TS_1, 'age')
#     0.0681873374585058
# >>> gain(TS_1, 'astigmatism')
#     0.21881472361493526
# >>> gain(TS_2, 'sky')
#     0.2467498197744391
# >>> gain(TS_2, 'wind')
#     0.04812703040826927
###############################################################################

def gain(D, A):
  # The gain is the starting entropy minus the sum of the attribute classification
  # entropies, weighted to the portion of D they classify
  dataset_labels, dataset_values = D[0], D[1]
  attribute_index = dataset_labels.index(A) # can throw
  attribute_values = set([a[attribute_index] for a in dataset_values])

  # Calculate starting entropy
  starting_D = len(dataset_values)
  starting_P = len([a for a in dataset_values if a[-1] == "yes"])
  starting_N = len([a for a in dataset_values if a[-1] == "no"])

  # Attribute map, storing for each attribute the positive / negative elements
  # ie. m["young"] = {"P": 10, "N": 2}
  attribute_map = {}
  for attribute_value in attribute_values:
    subset_for_attribute_value = [a for a in dataset_values if a[attribute_index] == attribute_value]
    P = len([a for a in subset_for_attribute_value if a[-1] == "yes"])
    N = len([a for a in subset_for_attribute_value if a[-1] == "no"])
    attribute_map[attribute_value] = {"P": P, "N": N, "D": P + N}

  # Return the gain
  gain_value = entropy(starting_P, starting_N)
  for attribute_value, classification in iter(attribute_map.items()):
    gain_value -= (classification["D"] / starting_D) * entropy(classification["P"], classification["N"])

  return gain_value

# print(gain(TS_1, "age"))
# print(gain(TS_1, "astigmatism"))
# print(gain(TS_2, "sky"))
# print(gain(TS_2, "wind"))

###############################################################################
# Exercise 3:
# 
# Define the function new_TS(TS, attribute, value). It receives as input
# a training set TS, an attribute and the value of that attribute, and returns
# a new training set formed from that TS, populated only with the examples in which
# the attribute takes the value. Also, the attribute should disappear from the
# attribute list, and its values should be eliminated from the example list.
# 
# Examples:

    >>> new_TS(TS_1, 'astigmatism', 'yes')
    [['age', 'tear_rate', 'lenses'],
    [['young', 'increased'  , 'yes'],
     ['young', 'normal', 'yes'],
     ['pre_presbyopia', 'reduced', 'no'],
     ['pre_presbyopia', 'increased', 'yes'],
     ['presbyopia', 'reduced', 'no'],
     ['presbyopia', 'normal', 'yes']]]

    >>> new_TS(TS_2, 'sky', 'sunny')
    [['temperature', 'humidity', 'wind', 'practice_sport'],
    [['high', 'high', 'weak', 'no'],
     ['high', 'high', 'strong', 'no'],
     ['mild', 'high', 'weak', 'no'],
     ['low', 'normal', 'weak', 'yes'],
     ['mild', 'normal', 'strong', 'yes']]]

###############################################################################

def new_TS(TS, attribute, value):
  dataset_labels, dataset_values = TS[0], TS[1]
  attribute_index = dataset_labels.index(attribute) # can throw

  # Let's form the new training set attributes
  ts_attributes = [a for a in dataset_labels if a != attribute]

  # And the new training set values
  # Only add the elements prior and after the desirted attribute
  ts_values = [a[0:attribute_index] + a[attribute_index+1:] for a in dataset_values if a[attribute_index] == value]

  return [ts_attributes, ts_values]

# print(new_TS(TS_1, 'astigmatism', 'yes'))
# print(new_TS(TS_2, 'sky', 'sunny'))

###############################################################################
# Exercise 4:
# A tree is a list that only has one classification label,
# or a list where its first element is the root node, and the rest of
# elements are the branches.
# A branch should have as first element the value of the node, followed
# by its corresponding tree (see examples).
#
# Define the function decision_tree(TS) that takes as input a training set TS,
# and returns a decision tree learnt by using the ID3 algorithm.
#
# Examples:
# >>> decision_tree(TS_1)
# ['astigmatism', ['yes', ['tear_rate', ['reduced'  , ['no']],
#                                       ['increased'  , ['yes']],
#                                       ['normal', ['yes']]]],
#                  ['no', ['age', ['young' , ['no']],
#                                 ['pre_presbyopia', ['tear_rate', ['reduced', ['yes']],
#                                                                  ['normal', ['no']]]],
#                                 ['presbyopia', ['no']]]]]
# >>> decision_tree(TS_2)
# ['sky', ['sunny', ['humidity', ['high'  , ['no']],
#                                   ['normal', ['yes']]]],
#           ['foggy', ['yes']],
#           ['rainy' , ['wind', ['weak' , ['yes']],
#                               ['strong', ['no']]]]]
###############################################################################

def decision_tree(TS):
  dataset_labels, dataset_values = TS[0], TS[1]

  # 1. All examples are positive
  # 2. All examples are negative
  classification_values = set([a[-1] for a in dataset_values])
  if len(classification_values) == 1:
    value = classification_values.pop()
    return [value]

  # 3. Attributes is empty
  if len(dataset_labels) == 0:
    classification_frequency = []
    for v in classification_values:
      classification_frequency.append((v, len([a for a in dataset_values if a[-1]==v])))
    return sorted(classification_frequency, key=lambda x: x[1], reverse=True)[0][0]

  # 4. Else
  # 4.1 Get the best attribute
  gain_by_attributes = []
  for attribute in dataset_labels[:-1]:
    gain_by_attributes.append((attribute, gain(TS, attribute)))
  best_attribute = sorted(gain_by_attributes, key=lambda x: x[1], reverse=True)[0][0]
  # print(gain_by_attributes, best_attribute)

  # 4.2 Create tree labeled with "attribute"
  best_attribute_index = dataset_labels.index(best_attribute)
  best_attribute_values = set([a[best_attribute_index] for a in dataset_values])
  forming_tree = [best_attribute]
  for v in best_attribute_values:
    forming_tree.append([v, decision_tree(new_TS(TS, best_attribute, v))])

  return forming_tree

# print(decision_tree(TS_1))
# print(decision_tree(TS_2))

###############################################################################
# Exercise 5:
# An instance is a list of lists, where the first element is the attribute list,
# and the second element is the list of values. For example:

i_1 = [['age', 'astigmatism', 'tear_rate'], 
       ['young', 'yes', 'reduced']]

i_2 = [['sky', 'temperature', 'humidity', 'wind'],
       ['sunny', 'high', 'normal', 'weak']]

# Define the function classify(tree, instance) that receives as input a tree and
# an instance (with the same attributes) and returns its classification.
# 
# Examples:
# >>> tree1 = decision_tree(TS_1)
# >>> classify(tree1, i_1)
#    'no'
# >>> tree2 = decision_tree(TS_2)
# >>> classify(tree2, i_2)
#    'yes'
###############################################################################

def recursive_tree_classify(tree, instance):
  # Base case: Return yes or no
  if tree[0] in ["yes", "no"]:
    return tree[0]

  # Traverse the tree and fetch the attribute value
  attribute = tree[0]
  dataset_labels, dataset_values = instance[0], instance[1]
  attribute_index = dataset_labels.index(attribute)
  attribute_value = dataset_values[attribute_index]

  # Search the element in the tree that gives attribute = attribute_value
  for subtree in tree[1:]:
    if subtree[0] == attribute_value:
      return recursive_tree_classify(subtree[1], instance)

def classify(tree, instance):
  return recursive_tree_classify(tree, instance)

# tree1 = decision_tree(TS_1)
# print(classify(tree1, i_1))
# tree2 = decision_tree(TS_2)
# print(classify(tree2, i_2))

###############################################################################
# Exercise 6:
# Define the function covered(TS, rule). The inputs are a training set TS and
# a rule. It returns the list [p, t] where p is the number of correctly covered
# examples, and t the total number of covered examples (correctly or not).
#
# We represent a rule as a list of two elements [premise, conclusion]. The premise
# is a list of pairs [attribute, value], whereas the conclusion is a single pair [attribute, value].
# For instance, the following rule r_1:
#
# if astigmatism = yes and tear_rate = normal, then lenses = hard
#
# is represented as:

r_1 = [[['astigmatism', 'yes'], ['tear_rate', 'normal']], ['lenses', 'hard']]

# Here is another example r_2

r_2 = [[['lenses', 'soft'],['tear_rate', 'normal'], ['astigmatism', 'no']], ['age', 'young']]

# Examples:
# >>> covered(TS_3, r_1)
# (4, 6)
# >>> covered(TS_3, r_2)
# (2, 5)
###############################################################################

def recursive_rule_classify(TS, premise, conclusion):
  # Base case: We have no more premise to apply.
  # Check how many examples are covered and how many
  # are covered correctly.
  if len(premise) == 0:
    dataset_labels, dataset_values = TS[0], TS[1]
    attribute_index = dataset_labels.index(conclusion[0])
    examples_with_value = [a[attribute_index] for a in dataset_values]
    total_covered = len(examples_with_value)
    correctly_covered = len([a for a in examples_with_value if a == conclusion[1]])
    return [correctly_covered, total_covered]

  # Else, exhaust the premise one by one recursively
  current_condition = premise[0]
  return recursive_rule_classify(new_TS(TS, current_condition[0], current_condition[1]), premise[1:], conclusion)

def covered(TS, rule):
  premise, conclusion = rule[0], rule[1]
  return recursive_rule_classify(TS, premise, conclusion)

# print(covered(TS_3, r_1))
# print(covered(TS_3, r_2))
